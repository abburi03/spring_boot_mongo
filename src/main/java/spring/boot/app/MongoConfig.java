package spring.boot.app;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@Configuration
@EnableMongoRepositories(basePackages = "")
public class MongoConfig extends AbstractMongoConfiguration {



    @Override
    protected String getDatabaseName() {
        return "testDB";
    }

    @Override
    public Mongo mongo() throws Exception {
        String dataBase = "localhost";

        return new MongoClient(dataBase, 27017);
    }


}